<?php

/**
 * @file
 * Administrative forms for the ESPN module.
 */

/**
 * ESPN admin settings form.
 */
function espn_admin_form() {
  drupal_add_css(drupal_get_path('module', 'espn') . '/espn.css');

  $form['espn_apikey'] = array(
    '#title' => t('ESPN API key'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('To request an API key, visit ESPN\'s <a href="@espn_url">developer site</a>.', array('@espn_url' => 'http://developer.espn.com/member/register')),
    '#default_value' => variable_get('espn_apikey', ''),
  );

  $form['espn_insider_content'] = array(
    '#title' => t('Show ESPN Insider content !insider_image', array('!insider_image' => theme('image', array('path' => 'http://a.espncdn.com/icons/in.gif')))),
    '#type' => 'checkbox',
    '#description' => t('ESPN Insider content is only available to certain users that have paid for premium content.'),
    '#default_value' => variable_get('espn_insider_content', TRUE),
  );

  // Gather and format available logos
  $logos = espn_logos();
  foreach ($logos as $name => $logo) {
    $logos[$name] = theme('image', $logo);
  }

  $form['espn_logo_fieldset'] = array(
    '#title' => t('ESPN logo'),
    '#type' => 'fieldset',
    '#description' => t('In order to use the ESPN API, you must also use an ESPN logo in accordance with their <a href="@espn_branding_url">branding policy</a>.', array('@espn_branding_url' => 'http://developer.espn.com/branding')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['espn_logo_fieldset']['espn_logo'] = array(
    // '#title' => t('ESPN logo'),
    '#type' => 'radios',
    '#required' => TRUE,
    '#options' => $logos,
    '#default_value' => variable_get('espn_logo', 'powered-by-espn-red_150'),
    '#theme' => 'espn_logo_grid',
    '#columns' => 5,
  );

  return system_settings_form($form);
}

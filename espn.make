api = 2
core = 7.x

projects[] = drupal

libraries[espn][download][type] = "get"
libraries[espn][download][url] = "https://github.com/madcaplaughs/PHP-SDK-for-ESPN-API/zipball/master"
libraries[espn][directory_name] = "PHP-SDK-for-ESPN-API"

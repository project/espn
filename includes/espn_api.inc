<?php

/**
 * @file
 * Contains EspnApi.
 */

/**
 * Defines a connector for the ESPN API.
 */
class EspnApi {

  /**
   * Denotes the primary URL endpoint for all API calls.
   */
  const ESPN_API_ENDPOINT = 'http://api.espn.com/v1';

  /**
   * Denotes a default number of records to show for each API call.
   */
  const ESPN_DEFAULT_LIMIT = 5;

  /**
   * Retrieve a list of all available sports.
   *
   * @see http://developer.espn.com/overview#helper-api-calls
   *
   * @return array
   *   An array of sports information, and FALSE if the API call fails.
   */
  function getSports() {
    $sports = array();

    try {
      $result = $this->call('sports');

      foreach ($result['sports'] as $sport) {
        $sports[$sport['name']] = $sport['name'];
      }
      return $sports;
    }
    catch (EspnApiException $e) {
      return FALSE;
    }
  }

  /**
   * Retrieve a list of all available leagues within a sport.
   *
   * @see http://developer.espn.com/overview#helper-api-calls
   *
   * @param string $sport
   *   (Optional) Use to get a list of all organizing bodies within a sport.
   *
   * @return array
   *   An array of sports information, and FALSE if the API call fails.
   */
  function getLeagues($sport) {
    $leagues = array(); 

    try {
      $result = $this->call("sports/$sport");

      if (isset($result['sports'][0]['leagues'])) {
        foreach ($result['sports'][0]['leagues'] as $league) {
          $leagues[$league['abbreviation']] = $league['name'];
        }
      }
      return $leagues;
    }
    catch (EspnApiException $e) {
      return FALSE;
    }
  }

  /**
   * Retrieve a list of all available divisions within a sport.
   *
   * @see http://developer.espn.com/overview#helper-api-calls
   *
   * @param string $sport
   *   (Optional) Use to get a list of all organizing bodies within a sport.
   *
   * @return array
   *   An array of sports information, and FALSE if the API call fails.
   */
  function getDivisions($sport, $league) {
    $divisions = array();
  
    try {
      $result = $this->call("sports/$sport/$league");

      if (isset($result['sports'][0]['leagues'][0]['groups'])) {
        foreach ($result['sports'][0]['leagues'][0]['groups'] as $division) {
          if (isset($division['abbreviation'])) {
            $divisions[$division['abbreviation']] = $division['name'];

            // Add sub-divisions (if they exist)
            if (isset($division['groups'])) { 
              foreach ($division['groups'] as $subdivision) {
                $divisions[$subdivision['groupId']] = '-- ' . $subdivision['name'];
              }
            }
          }
        }
      }

      return $divisions;
    }
    catch (EspnApiException $e) {
      return FALSE;
    }
  }

  /**
   * Retrieve a list of all news and headlines for a sport.
   *
   * @see http://developer.espn.com/docs/headlines#parameters
   *
   * @param string $sport
   *   A sport (and optionally league) to filter.
   * @param string $groups
   *   A set of groups (divisions) to include.
   * @param string $limit
   *   (Optional) Use to override the default number of results returned.
   * @param bool $headlines_only
   *   (Optional) Show only top stories as selected by ESPN editorial staff.
   *
   * @return array
   *   An array of news items, and FALSE if the API call fails.
   */
  function getNews($sport, $group = '', $limit = EspnApi::ESPN_DEFAULT_LIMIT, $headlines_only = FALSE) {
    $resource = 'sports/' . $sport;
		if (!empty($group)) {
			$resource .= "/$group";
		}
    $method = 'news';
    if ($headlines_only) {
      $method .= '/headlines';
    }

    $result = $this->call($resource, $method, array('limit' => $limit));
    return $result ? $result['headlines'] : $result;
  }

  /**
   * Perform an ESPN API call.
   *
   * @see http://developer.espn.com/overview#technical-overview
   *
   * @param string $resource
   *   An ESPN API resource type.
   * @param string $method
   *   (Optional) An ESPN API method. Omit for helper API calls.
   * @param array $params
   *   (Optional) Additional GET parameters to add to the request.
   *
   * @return array
   *   An array of results, and FALSE if the API call fails.
   */
  private function call($resource, $method = '', $params = array()) {
    // Add API key to params
    $params['apikey'] = variable_get('espn_apikey', '');

    // Don't do anything if API key is not set
    if (empty($params['apikey'])) {
      return FALSE;
    }

    // Add other global paramters
    if (!variable_get('espn_insider_content', TRUE)) {
      $params['insider'] = 'no';
    }

    // Force ESPN API to return a result in JSON format
    $headers = array('Accept' => 'application/json');

    // Build URL parameters
    $data = array();
    foreach ($params as $key => $value) {
      $data[] = rawurlencode($key) . '=' . rawurlencode($value);
    }

    // Build request URL
    $url = EspnApi::ESPN_API_ENDPOINT . "/$resource" . ($method ? "/$method" : '') . '?' . implode('&', $data);

    if (variable_get('espn_debug', FALSE)) {
      drupal_set_message(t('ESPN API call => !url', array('!url' => $url)));
    }

    $result = drupal_http_request($url, array(
        'headers' => $headers,
    ));

    // Respond to the result code
    // @see http://developer.espn.com/overview#http-codes
    switch ($result->code) {
      case 200:
        return drupal_json_decode($result->data);
      case 400: // Bad Request
      case 401: // Unauthorized
      case 403: // Account Over Rate Limit
      case 404: // Not Found
      case 500: // Internal Server Error
      case 504: // Gateway Timeout
        watchdog('espn', 'Received error code @code (%status_msg) from ESPN API.', array(
          '@code' => $result->code,
          '%status_msg' => $result->status_message,
          ),
          WATCHDOG_ERROR,
          $url
        );
        throw new EspnApiException();
        return FALSE;
    }
  }

}

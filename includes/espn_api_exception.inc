<?php

/**
 * @file
 * Contains EspnApiException.
 */

/**
 * Defines an exception class for the ESPN API.
 */
class EspnApiException extends Exception { }

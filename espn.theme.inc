<?php

/**
 * @file
 * Theme functions for the ESPN module.
 */

/**
 * Output a list of options in the number of columns specified by the element's
 * #columns value.
 * @see http://drupal.org/node/1010284
 */
function theme_espn_logo_grid($variables) {
  // Modify to dig one layer deeper.  Not sure why original code didn't need this.
  $stripped_element = array_values($variables);
  $variables = $stripped_element[0];

  // Initialize variables.
  $output = '';
  $total_columns = $variables['#columns'];
  $total_options = count($variables['#options']);
  $options_per_column = ceil($total_options / $total_columns);
  $keys = array_keys($variables['#options']);
  $type = $variables[$keys[0]]['#type'];

  // Start wrapper div.
  $output .= '<div class="multicolumn-options-wrapper">';
  $current_column = 1;
  $current_option = 0;

  while ($current_column <= $total_columns) {
    // Start column div.
    $output .=  '<div class="multicolumn-options-column" style="float: left">';

    // Keep looping through until the maximum options per column are reached,
    // or you run out of options.
    while ($current_option < $options_per_column * $current_column &&
           $current_option < $total_options) {

      // Output as either check or radio button depending on the element type.
      $output .= drupal_render($variables[$keys[$current_option]]);
      $current_option++;
    }

    // End column div.
    $output .= '</div>';
    $current_column++;
  }

  // End wrapper div.
  $output .= '</div>';
  $output .= '<div class="clear-block"></div>';

  return $output;
}

function theme_espn_image($variables) {
  $element = $variables['image'];
  
  if (!isset($variables['attributes'])) {
    $variables['attributes'] = array();
  }
  
  $variables['attributes']['class'] = 'espn-image-wrapper';

  $options = array(
    'path' => $element->url,
    'height' => $element->height,
    'width' => $element->width,
    'alt' => $element->alt,
    'class' => 'espn-image',
    'caption' => $element->caption,
  );

  return '<div ' . drupal_attributes($variables['attributes']) . '>' . theme('image', $options) . '</div>';
}

<div class="espn-news-item">
  <span class="espn-news-item-title"><?php print $headline; ?></span>
  <?php if ($show_description): ?>
  <span class="espn-news-item-description"><?php print $description; ?></span>
  <?php endif; ?>
</div>
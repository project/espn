<?php

/**
 * @file
 * Theme functions for the ESPN Bean module.
 */

/**
 * Implements hook_preprocess_HOOK() for theme_espn_news_item().
 */
function espn_bean_preprocess_espn_news_item(&$variables) {
  $variables['headline'] = l($variables['item']['headline'], $variables['item']['links']['web']['href']);
  $variables['description'] = $variables['item']['description'];
}

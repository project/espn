<?php
/**
 * @file
 * ESPN headlines plugin.
 */

class EspnNewsBean extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    $values = array(
      'settings' => array(
        'sport' => '',
        'league' => '',
        'division' => array(),
        'limit' => 5,
        'headlines' => FALSE,
        'show_description' => TRUE,
      ),
    );

    return array_merge(parent::values(), $values);
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = array();
    $form['settings'] = array(
      '#type' => 'fieldset',
      '#tree' => 1,
      '#title' => t('Resource'),
    );

    // Add common resource selection form.
    espn_bean_sports_form($bean, $form, $form_state);

    // News & Headlines API does not currently support limiting by division.
    // @see http://developer.espn.com/forum/read/159561
    if (isset($form['settings']['league']['#ajax'])) {
      unset($form['settings']['league']['#ajax']);
    }
    if (isset($form['settings']['division'])) {
      unset($form['settings']['division']);
    }

    $form['settings']['limit'] = array(
      '#type' => 'textfield',
      '#title' => t('# of Headlines'),
      '#description' => t('Used to limit the number of results returned.'),
      '#default_value' => $bean->settings['limit'],
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
    );
    $form['settings']['headlines'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show only top stories as selected by ESPN editorial staff.'),
      '#default_value' => $bean->settings['headlines'],
    );
    $form['settings']['show_description'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show short description for each news item.'),
        '#default_value' => $bean->settings['show_description'],
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $espn = new EspnApi();
    $items = array();

    $news = $espn->getNews($bean->settings['sport'], $bean->settings['league'], $bean->settings['limit'], $bean->settings['headlines']);
    foreach ($news as $item) {
      $items[] = theme('espn_news_item', array('item' => $item, 'show_description' => $bean->settings['show_description']));
    }
    return theme('espn_news', array('news' => $items));
  }
}
